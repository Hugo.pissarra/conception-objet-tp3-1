class Thing(object):
    
    
    def __init__(self,vol,nom=""):
        self._volume = vol
        self._nom = nom;
    
    @staticmethod
    def from_data (data):
        v = data.get("cleVolume", None)
        n = data.get("cleNom" , None)
        return Thing (vol=v, nom=n)       

    def getVolume(self):
        return self._volume

    def __repr__(self):
        return self._nom
    
    def repr (self):
        return self._nom
    
    def set_name(self,nom):
        self._nom = nom
        
    def has_name(self,nomDonne):
        return (self._nom == nomDonne)
        


class Box (object):
    
    
    
    def __init__(self, ouvert=True, capacity=None, cle=""):
        self._contents = []
        self._ouverte = ouvert
        self._capacity = capacity
        self._cle = cle
    
    @staticmethod
    def from_data (data):
        o = data.get("Ouverture", None)
        c = data.get("Capacite" , None)
        return Box (ouvert=o, capacity=c)     
    
    def __contains__(self,machin):
        return machin in self._contents
    
    
    
    
    def add (self, truc):
        self._contents.append(truc)
    
    def retirerTruc (self, truc):
        self._contents.remove(truc)    
        
    
    
    
        
    def estOuverte(self):
        return self._ouverte
    
    #def ouvrir(self):
        #self._ouverte = True
        
    def fermer(self):
        self._ouverte = False
    
     
    # retourne une phrase indiquant le contenu d'une boite   
    def action_look(self):
        if self.estOuverte() == True:
            res = "La boite contient : " + ", ".join(self._contents)
        else:
            res = "La boite est fermee"
        return res
    
    
    
    def getCapacity(self):
        return self._capacity
    
    def setCapacity(self,volumeAttribue):
        self._capacity = volumeAttribue
    
     
    # retourne true si assez de place pour rajouter une chose dans une boite ouverte
    # sinon false
    def has_room_for(self,chose):
        if self.getCapacity() == None:
            return True
        else:
            return (chose.getVolume() < self.getCapacity())
        
    
    def action_add(self,t):
        if self.estOuverte() == True:
            if self.has_room_for(t) == True:
                self._contents.append(t)
                return True
            else:
                return False
        else:
            return False
        
    
    # retourne le truc demande si dans la boite
    # sinon none
    def find(self,nomTruc):
        if self.estOuverte() == True:
            res = None
            i = 0
            while res == None and i < len(self._contents):
                if self._contents[i].has_name(nomTruc):
                    res = nomTruc
                i += 1
            return res
        else:
            return None
    
    
    def set_key(self,t):
        self._cle = t
        # t ==> Thing
        
    def open_with(self,t):
        if self.estOuverte() == False and t == self._cle:
            self._ouverte = True
        
            
        
        

def list_from_yaml(dataL):
    res = []
    for element in dataL:
        if element["type"] == "box":
            res.append(Box.from_data(element))
        if element["type"] == "thing":
            res.append(Thing.from_data(element))
    return res
    
    

