#test modification

from tp2 import *

def test_box_create():
    ##### class box
    b = Box()

    # tests add
    b.add("truc1")
    b.add("truc2")
    b.add("truc3")

    # tests contains
    assert ("truc1" in b) == True
    assert ("truc25" in b) == False

    # test retirer
    assert("truc3" in b) == True
    b.retirerTruc("truc3")
    assert("truc3" in b) == False


    # tests action_look (contenu d'une boite)
    #assert(b.action_look()=="La boite contient : truc1, truc2")
    #b.fermer()
    #assert(b.action_look()=="La boite est fermee")
    #b.ouvrir()
    #assert(b.action_look()=="La boite contient : truc1, truc2")
    #pass


    ##### ajout de la class Thing
def tests_box_thing():
    b2 = Box()
    b3 = Box(True,5)

    chose1 = Thing(3)
    chose2 = Thing(9)

    # tests getVolume
    assert (chose1.getVolume() == 3)
    assert (chose2.getVolume() == 9)

    # tests getCapacity, setCapacity
    assert(b2.getCapacity() == None)
    b2.setCapacity(5)
    assert(b2.getCapacity() == 5)
    b2.setCapacity(None)
    assert(b2.getCapacity() == None)

    assert(b3.getCapacity() == 5)

    # tests has_room_for (possibilite d'ajout d'une chose si capacite restante satisfaisante)
    assert (b2.has_room_for(chose1) == True)
    assert (b2.has_room_for(chose2) == True)

    assert (b3.has_room_for(chose1) == True)
    assert (b3.has_room_for(chose2) == False)

    # tests action_add(t) (ajoute un t a une boite si place suffisante)
    #assert b2.action_add(chose1) == True
    #assert b2.action_add(chose2) == True

    #assert b3.action_add(chose1) == True
    #b3.fermer()
    #assert b3.action_add(chose1) == False
    #b3.ouvrir()
    #assert b3.action_add(chose1) == True
    #assert b3.action_add(chose2) == False


    # tests __repr__, repr, set_name(nom) et has_name(nom)
    assert chose1.repr() == ""
    chose1.set_name("c1")
    assert chose1.repr() == "c1"
    assert chose1.has_name("c1") == True
    assert chose1.has_name("") == False

    chose2.set_name("c2")
    assert chose2.repr() == "c2"


    # tests find

    b4 = Box(True,15)
    chose3 = Thing(3,"toto")
    chose4 = Thing(4, "tata")
    chose5 = Thing(29,"tutu")

    b4.action_add(chose3)
    b4.action_add(chose4)
    b4.action_add(chose5)

    b4.find("toto") == chose3
    b4.find("tata") == chose4
    b4.find("tutu") == None
    b4.find("titi") == None

    b4.fermer()
    b4.find("toto") == None






def tests_yaml():
    exT = Thing.from_data({"cleVolume":9, "cleNom":"machin"})
    assert exT.repr() == "machin"
    assert exT.getVolume() == 9

    exB = Box.from_data({"Ouverture":True, "Capacite":10})
    exB.estOuverte()
    assert exB.getCapacity()==10


    dataDonnees = [ {"cleVolume":9, "cleNom":"machin", "type":"thing"}, {"cleVolume":9, "cleNom":"chose", "type":"thing"},  {"Ouverture":True, "Capacite":10, "type":"box"}, {"Ouverture":False, "Capacite":9, "type":"box"}]

    liste = list_from_yaml(dataDonnees)
    assert (len(liste)==4)



def tests_tp3():
    boite = Box ()
    cleChose = Thing(9,"c")
    boite.fermer()
    boite.open_with(cleChose)
    assert boite.estOuverte() == False
    # on a pas configurer cleChose en tant que cle

    boite.set_key(cleChose)
    boite.open_with(cleChose)
    assert boite.estOuverte() == True
